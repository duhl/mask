<?php

namespace duhl\utils;

class mask {

	private $mask = 0;
	private $bit64 = false;

	public function __construct($value = 0) {
		$this->set($value);
		$this->bit64 = $this->is64();
	}

	public function __destruct() {
	}
	
	/* active les bits a 1 de $value */
	public function bitOn($value) {
	  if ( ! is_int($value)) {
	    throw new \InvalidArgumentException('Bad type:'.gettype($value).', INTEGER expected.');
	  }
	  $this->mask |= ($value);
		return $this;
	}

	/* desactive les bits a 1 de $value */
	public function bitOff($value) {
	  if ( ! is_int($value)) {
	    throw new \InvalidArgumentException('Bad type:'.gettype($value).', INTEGER expected.');
	  }
	  $this->mask &= ~($value);
		return $this;
	}

	/* verifie si les bits a 1 du mask correspondes aux bits a 1 de $value */
	public function is($value) {
	  if ( ! is_int($value)) {
	    throw new \InvalidArgumentException('Bad type:'.gettype($value).', INTEGER expected.');
	  }
		return ( ($this->mask & ($value) ) === $value );
	}

	/* remplace les mask par $value */
	public function set( $value) {
	  if ( ! is_int($value)) {
	    throw new \InvalidArgumentException('Bad type:'.gettype($value).', INTEGER expected.');
	  }
	  $this->mask = $value;
		return $this;
	}

	public function get() {
		return $this->mask;
	}
	
	/* fork le masque pour y appliquer des changements sans modifier l'objet original,chainable (recommandé) */
	public function lock() {
		$tmp = new mask($this->mask);
		return $tmp;
	}
	
	private function is64() {
	  $large_number = 9223372036854775807;
	  return is_int($large_number);
	}
	public function printMask() {
	  echo str_pad( decbin( $this->mask ), ( $this->bit64 ? 64 : 32 ), "0", STR_PAD_LEFT) . PHP_EOL;
	}
	/*
	** renvoi true si un et un seul bit est actif
	*/
	public static function isFlag($value) {
	  if ( ! is_int($value)) {
	    throw new \InvalidArgumentException('Bad type:'.gettype($value).', INTEGER expected.');
	  }
		for ( $value += 0 ; $value > 0 && ($value & 0x1) !== 0x1 ; $value >>= 1 );
		if ( $value == 0) {
			return false;
		}
		$value >>= 1;
		return ( $value == 0 );
	}
	
}
